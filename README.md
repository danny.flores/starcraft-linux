# INSTALL STARCRAFT ON LINUX WITH WINE

Before install remove all installations of Wine that are not staging.

## Install wine-stage

Install wine-stage and install all dependencies

### Ubuntu
```bash
sudo add-apt-repository ppa:wine/wine-builds
sudo apt-get update
sudo apt install wine-staging
```
### Arch

Install wine-staging from AUR

## Install ms fonts

### Ubuntu

```bash
sudo apt install ttf-mscorefonts-installer
```

### Arch

Install ttf-ms-fonts with pacman or AUR

## Configure wine

Configure libraries on wine

Launch winecfg but search wine staging

```bash
whereis winecfg
```

Use installed winecfg in wine-staging folder


![libraries configuration](wine_configurations_0.png)

```bash
api-ms-win-crt-math-l1-1-0
api-ms-win-crt-stdio-l1-1-0
dgbhelp
msvcp140
ucrtbase
vcruntime140
```

![Stage configuration](wine_configurations_1.png)

Go to .wine folder in your home and copy the installer file into the drive_c folder

You can download the installer from battle.net 

https://www.battle.net/download/getInstallerForGame?os=win&locale=enUS&version=LIVE&gameProgram=STARCRAFT&id=535335976.1518026534

and download to wine drive C

Execute the installer
```bash
WINEDEBUG=-all vblank_mode=0 wine .wine/drive_c/StarCraft-Setup.exe
```

wait until battle net is installed correctly

go to `/home/user/.wine/drive_c/Program Files (x86)/Battle.net` in a terminal

and execute
```bash
WINEDEBUG=-all vblank_mode=0 wine Battle.net.exe
```

Install SC or copy the game binaries into  `/home/user/.wine/drive_c/Program Files (x86)`
